//
//  CartHeaderCell.swift
//  FoodApp
//
//  Created by Macroid on 7/15/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import UIKit

protocol CartHeaderDelegate {
    func backArrowTapped()
}

class CartHeaderCell: UITableViewCell {

    @IBOutlet weak var backArrowImgView:UIImageView!
    @IBOutlet weak var totalLablel:UILabel!
    
    static let cellIdentifier = "CartHeaderCell"
    var total:Double!{
        didSet{
            configure()
        }
    }
    var cartHeaderDelegate:CartHeaderDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let tapGessture = UITapGestureRecognizer(target: self, action: #selector(backArrowImgeViewTapped))
        backArrowImgView.isUserInteractionEnabled = true
        backArrowImgView.addGestureRecognizer(tapGessture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func nib()-> UINib{
        return UINib(nibName: CartHeaderCell.cellIdentifier, bundle: nil)
    }
    
    func configure(){
        totalLablel.text = "\(AppConfig.currencySymbol) \(total.format(f: ".2"))"
    }
       
    @objc
    func backArrowImgeViewTapped(){
        cartHeaderDelegate.backArrowTapped()
    }
    
}
