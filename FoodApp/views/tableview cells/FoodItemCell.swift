//
//  FoodItemCell.swift
//  FoodApp
//
//  Created by Macroid on 7/14/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import UIKit

enum ActionType:Int {
    case Add
    case Plus
    case Minus
}

protocol FoodItemCellDelegate {
    func foodItemTapped(actionType:ActionType, indexPath:IndexPath)
}

class FoodItemCell: UITableViewCell {

    static let cellIdentifier = "FoodItemCell"
    
    @IBOutlet weak var foodNameLbl:UILabel!
    @IBOutlet weak var foodDescriptionLbl:UILabel!
    @IBOutlet weak var foodPriceLbl:UILabel!
    @IBOutlet weak var plusBtn:UIButton!
    @IBOutlet weak var minusBtn:UIButton!
    @IBOutlet weak var quantityLbl:UILabel!
    @IBOutlet weak var chatImageView:UIImageView!
    
    var indexPath:IndexPath!
    var foodItemCellDelegate:FoodItemCellDelegate?
    var foodViewModel:FoodItemCellViewModel!{
        didSet{
            configure()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        plusBtn.addTarget(self, action: #selector(actionBtnTapped(_:)), for: .touchUpInside)
        minusBtn.addTarget(self, action: #selector(actionBtnTapped(_:)), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func nib()-> UINib{
        return UINib(nibName: FoodItemCell.cellIdentifier, bundle: nil)
    }
    
    func configure(){
        let food = foodViewModel.food
        foodNameLbl.text = food.name
        foodDescriptionLbl.text = food.description
        foodPriceLbl.text = "\(AppConfig.currencySymbol) \(food.price)"
        quantityLbl.text = "\(food.sessionQuantity)"
        
        if food.sessionQuantity == 0{
            plusBtn.setTitle("ADD", for: .normal)
            minusBtn.isHidden = true
            quantityLbl.isHidden = true
            plusBtn.isHidden = false
            plusBtn.tag = 1
        }else{
            plusBtn.setTitle("+", for: .normal)
            plusBtn.isHidden = false
            minusBtn.isHidden = false
            quantityLbl.isHidden = false
            plusBtn.tag = 2
            minusBtn.tag = 3
        }
    }
    
    func showChatImageView(){
        chatImageView.isHidden = false
    }
    
    @IBAction func actionBtnTapped(_ sender:UIButton){
        switch sender.tag {
        case 1:
            foodItemCellDelegate?.foodItemTapped(actionType: .Add, indexPath: indexPath)
        case 2:
            foodItemCellDelegate?.foodItemTapped(actionType: .Plus, indexPath: indexPath)
        default:
            foodItemCellDelegate?.foodItemTapped(actionType: .Minus, indexPath: indexPath)
        }
    }
}
