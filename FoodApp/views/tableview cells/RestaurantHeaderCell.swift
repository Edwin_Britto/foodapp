//
//  RestaurantHeaderCell.swift
//  FoodApp
//
//  Created by Edwin on 7/14/20.
//  Copyright © 2020 Edwin. All rights reserved.
//

import UIKit


class RestaurantHeaderCell: UITableViewCell {

    static let cellIdentifier = "RestaurantHeaderCell"
    @IBOutlet weak var ratingLbl:UILabel!
    @IBOutlet weak var mobileLbl:UILabel!
    @IBOutlet weak var bookBtn:UIButton!
    
    var restaurantCellModel:RestaurantCellViewModel!{
        didSet{
            configure()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func nib()-> UINib{
        return UINib(nibName: RestaurantHeaderCell.cellIdentifier, bundle: nil)
    }
    
    func configure(){
        let restaurant = restaurantCellModel.restaurant
        ratingLbl.text = "\(restaurant.overAllRating)(\(restaurant.totalRatingCount)+) | \(restaurant.activeTime)"
        mobileLbl.text = "Reach us at: \(restaurant.mobile)"
    }
    
}
