//
//  DeliveryOptionsCell.swift
//  FoodApp
//
//  Created by Macroid on 7/15/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import UIKit
import DLRadioButton

enum FoodItemViewType:Int{
    case showLess
    case showMore
}

protocol DeliveryOptionDelegate {
    func showMoreTapped(type:FoodItemViewType)
}

class DeliveryOptionsCell: UITableViewCell {

    static let cellIdentifier = "DeliveryOptionsCell"
    
    @IBOutlet weak var showMoreBtn:UIButton!
    var deliveryOptionDelegate:DeliveryOptionDelegate!
    var foodItemType:FoodItemViewType!{
        didSet{
            configureBtn()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    func configureBtn(){
        var text = ""
        if foodItemType == .some(.showMore){
            text = "Show More"
        }else{
            text = "Show Less"
        }
        let attributedString = NSAttributedString(string: NSLocalizedString(text, comment: ""), attributes:[
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0),
            NSAttributedString.Key.foregroundColor : UIColor.gray,
            NSAttributedString.Key.underlineStyle:1.0
        ])
        showMoreBtn.setAttributedTitle(attributedString, for: .normal)
        showMoreBtn.addTarget(self, action: #selector(showMoreBtnTaped), for: .touchUpInside)
    }
    
    @objc
    func showMoreBtnTaped(){
        deliveryOptionDelegate.showMoreTapped(type: foodItemType)
    }
    
    static func nib()-> UINib{
           return UINib(nibName: DeliveryOptionsCell.cellIdentifier, bundle: nil)
    }
}
