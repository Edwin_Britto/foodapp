//
//  BottomCartView.swift
//  bringmealsuser
//
//  Created by Macroid on 12/4/19.
//  Copyright © 2019 Macroid. All rights reserved.
//

import UIKit


class HeaderView: UIView{
    
    static let identifier:String = "HeaderView"
    @IBOutlet weak var headerLbl: UILabel!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: identifier, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
}

