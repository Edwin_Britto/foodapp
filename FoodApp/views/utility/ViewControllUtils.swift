//
//  ViewControllUtils.swift
//  FoodApp
//
//  Created by Macroid on 7/15/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    func animShow(){
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
                       animations: { self.view.layoutIfNeeded() }, completion: nil)
    }
    
    func animHide(){
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut],
                       animations: { self.view.layoutIfNeeded() }, completion: nil)
    }
    
}
