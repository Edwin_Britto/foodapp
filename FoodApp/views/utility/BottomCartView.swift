//
//  BottomCartView.swift
//  bringmealsuser
//
//  Created by Macroid on 12/4/19.
//  Copyright © 2019 Macroid. All rights reserved.
//

import UIKit


class BottomCartView: UIView{
    
    //@IBOutlet weak var cartTotalAmount:UILabel!
    @IBOutlet weak var cartTotalAmount: UILabel!
    @IBOutlet weak var bottomContentView: UIView!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "BottomCartView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
}

