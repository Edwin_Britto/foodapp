//
//  Colors.swift
//  FoodApp
//
//  Created by Macroid on 7/15/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation
import UIKit

enum AppTheme:String {
    case primaryColor
    case secondaryColor
    case blackColor
    
    var color : UIColor {
        switch self {
            case .primaryColor:
                return UIColor(named: "Primary")!
            case .secondaryColor:
                return UIColor(named: "Secondary")!
            case .blackColor:
                return UIColor(named: "Black")!
        }
    }
}
