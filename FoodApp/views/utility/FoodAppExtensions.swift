//
//  FoodAppExtensions.swift
//  FoodApp
//
//  Created by Macroid on 7/16/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation

extension Int{
    func toDouble() -> Double? {
        return Double(self)
    }
    
}

extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}
