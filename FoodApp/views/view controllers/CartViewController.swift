//
//  CartViewController.swift
//  FoodApp
//
//  Created by Macroid on 7/15/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import UIKit
import RxSwift

protocol CartViewDelegate {
    func screenDidClose(model:FoodListViewModel)
}

class CartViewController: UIViewController {
    
    var foodListViewModel:FoodListViewModel!
    var cartViewDelegate: CartViewDelegate!
    var cartListViewModel:CartListViewModel!
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var cartTableView:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        cartListViewModel = CartListViewModel(cartList: foodListViewModel.cartList)
        if cartListViewModel.cartList.count > 2{
            cartListViewModel.foodItemViewType = .showMore
        }else{
            cartListViewModel.foodItemViewType = .showLess
        }
        registerCells()
    }
    
    //registering tableview cells
    func registerCells(){
        cartTableView.register(CartHeaderCell.nib(), forCellReuseIdentifier: CartHeaderCell.cellIdentifier)
        cartTableView.register(FoodItemCell.nib(), forCellReuseIdentifier: FoodItemCell.cellIdentifier)
        cartTableView.register(DeliveryOptionsCell.nib(), forCellReuseIdentifier: DeliveryOptionsCell.cellIdentifier)
        cartTableView.tableFooterView = UIView()
        cartTableView.reloadData()
    }

}


extension CartViewController:UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0,2:
            return 1
        default:
            let cartItemCount = cartListViewModel.cartList.count
            if cartListViewModel.foodItemViewType == .some(.showMore){
                return (cartItemCount > 1 ) ? 2 : 1
            }else{
                return cartListViewModel.cartList.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: CartHeaderCell.cellIdentifier) as! CartHeaderCell
            cell.cartHeaderDelegate = self
            cell.total = cartListViewModel.fetchTotalPrice()
            cell.selectionStyle = .none
            return cell
        case 1:
            let model = cartListViewModel.cartList[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: FoodItemCell.cellIdentifier) as! FoodItemCell
            cell.showChatImageView()
            cell.foodViewModel = FoodItemCellViewModel(food: model)
            cell.indexPath = indexPath
            cell.foodItemCellDelegate = self
            cell.selectionStyle = .none
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: DeliveryOptionsCell.cellIdentifier) as! DeliveryOptionsCell
            cell.foodItemType = cartListViewModel.foodItemViewType
            cell.deliveryOptionDelegate = self
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 250
        case 1:
            return 90
        default:
            if cartListViewModel.cartList.count > 2{
                return 135
            }else{
                return 100
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let customView = HeaderView.instanceFromNib()
        let view = customView as? HeaderView
        view?.headerLbl.text = "Review Orders"
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0,2:
            return 0
        default:
           return 50
        }
    }
}

extension CartViewController:FoodItemCellDelegate, CartHeaderDelegate, DeliveryOptionDelegate{
    func foodItemTapped(actionType: ActionType, indexPath: IndexPath) {
        let position = indexPath.row
        cartListViewModel.updateFoodByAction(actionType: actionType,selectedIndex:position)
            .subscribe(onNext: { (updatedFood) in
                if updatedFood.sessionQuantity == 0{
                    self.foodListViewModel.updateCartListByFood(food: updatedFood)
                    if self.cartListViewModel.cartList.count >= 1 {
                       self.cartTableView.reloadData()
                    }else{
                       self.backArrowTapped()
                    }
                }else{
                    self.foodListViewModel.updateCartListByFood(food: updatedFood)
                 
                    if self.cartListViewModel.cartList.count == 0{
                        self.backArrowTapped()
                    }else{
                        //refresh cart item cell
                        self.updateViewsInCell(updatedFood: updatedFood, indexPath: indexPath)
                    }
                }
            }).disposed(by: disposeBag)
    }
    
    //refresh cart item cell
    func updateViewsInCell(updatedFood:Food,indexPath:IndexPath){
        guard let cell = cartTableView.cellForRow(at: indexPath) as? FoodItemCell else{
            return
        }
        cell.foodViewModel = FoodItemCellViewModel(food: updatedFood)
        
        //refresh cartheader
        cartTableView.reloadSections([0], with: .none)
    }
    
    //cart Delegate
    func backArrowTapped() {
        cartViewDelegate.screenDidClose(model: foodListViewModel)
        navigationController?.popViewController(animated: true)
    }
    
    //changing the food item view types
    func showMoreTapped(type:FoodItemViewType) {
        if type == .showLess{
            cartListViewModel.foodItemViewType = .showMore
        }else{
            cartListViewModel.foodItemViewType = .showLess
        }
        cartTableView.reloadSections([1,2], with: .none)
    }
}
