//
//  ViewController.swift
//  FoodApp
//
//  Created by Macroid on 7/14/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import UIKit
import RxSwift

class FoodListController: UIViewController {

    @IBOutlet weak var foodListTableView:UITableView!
    //this to close and open the cartview
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    //bottom cartview
    var bottomCartView: BottomCartView?
    var foodListViewModel:FoodListViewModel = FoodListViewModel()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // hide navigation
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        registerCells()
        registerBottomCartView()
        
        //fetch all foods from view model
        foodListViewModel.fetchAllFoods()
        
        foodListTableView.reloadData()
        
    }
    
    //registering tableview cells
    func registerCells(){
        foodListTableView.register(RestaurantHeaderCell.nib(), forCellReuseIdentifier: RestaurantHeaderCell.cellIdentifier)
        foodListTableView.register(FoodItemCell.nib(), forCellReuseIdentifier: FoodItemCell.cellIdentifier)
        foodListTableView.delegate = self
        foodListTableView.dataSource = self
        foodListTableView.tableFooterView = UIView()
    }
    
    //adding custom view
    func registerBottomCartView(){
          let customView = BottomCartView.instanceFromNib()
          bottomCartView = customView as? BottomCartView
          view.addSubview(customView)
          customView.translatesAutoresizingMaskIntoConstraints = false
          NSLayoutConstraint.activate([
              customView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
              customView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
              customView.heightAnchor.constraint(equalToConstant: 60)])
          bottomConst =  customView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 100)
          bottomConst.isActive = true
          let gesture = UITapGestureRecognizer(target: self, action: #selector(openViewCart))
          bottomCartView?.addGestureRecognizer(gesture)
      }
    
    @objc
    func openViewCart(){
        let vc = storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        vc.foodListViewModel = foodListViewModel
        vc.cartViewDelegate = self
        navigationController?.pushViewController(vc, animated: true)
    }

}

extension FoodListController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantHeaderCell.cellIdentifier) as! RestaurantHeaderCell
            let restaurantCellModel = RestaurantCellViewModel(restaurant: foodListViewModel.restaurantInfo)
            cell.restaurantCellModel = restaurantCellModel
            cell.selectionStyle = .none
            return cell
        default:
            let model = foodListViewModel.foods[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: FoodItemCell.cellIdentifier) as! FoodItemCell
            cell.foodViewModel = FoodItemCellViewModel(food: model)
            cell.indexPath = indexPath
            cell.foodItemCellDelegate = self
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 300
        default:
            return 90
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return foodListViewModel.foods.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let customView = HeaderView.instanceFromNib()
        let view = customView as? HeaderView
        view?.headerLbl.text = "Starter"
        return customView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        default:
           return 50
        }
    }
}

extension FoodListController:FoodItemCellDelegate{
    func foodItemTapped(actionType: ActionType, indexPath: IndexPath) {
        let position = indexPath.row
        let selectedFood = foodListViewModel.foods[position]
        foodListViewModel.updateFoodByAction(actionType: actionType, food: selectedFood)
            .subscribe(onNext: { (food) in
                self.foodListViewModel.foods[position] = food
                self.updateViewsInCell(updatedFood: food, indexPath: indexPath)
                self.refreshBottomView()
            }).disposed(by: disposeBag)
    }
    
    func updateViewsInCell(updatedFood:Food,indexPath:IndexPath){
        guard let cell = foodListTableView.cellForRow(at: indexPath) as? FoodItemCell else{
            return
        }
        cell.foodViewModel = FoodItemCellViewModel(food: updatedFood)
    }
    
    //refresh botttom view when the item changed
    func refreshBottomView(){
        let count = foodListViewModel.getOverAllItemsCount()
        if count == 1{
            bottomCartView?.cartTotalAmount.text = "VIEW CART ( \(count) ITEM)"
        }else{
            bottomCartView?.cartTotalAmount.text = "VIEW CART ( \(count) ITEMS)"
        }
        
        if foodListViewModel.cartList.count == 0{
            bottomConst.constant = 100
            animHide()
        }else if bottomConst.constant == 100{
            bottomConst.constant = 0
            animShow()
        }
    }
}

extension FoodListController: CartViewDelegate{
    //refresh food item when the cart screen closed
    func screenDidClose(model:FoodListViewModel){
        foodListViewModel = model
        for food in self.foodListViewModel.cartList{
            if let index = foodListViewModel.foods.firstIndex(where: {$0.id == food.id}){
                foodListViewModel.foods[index] = food
                let indexPath = IndexPath(row: index, section: 1)
                updateViewsInCell(updatedFood: food, indexPath: indexPath)
            }
        }
        foodListViewModel.cleanUpCartList()
        refreshBottomView()
    }
}

