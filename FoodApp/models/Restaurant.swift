//
//  Restaurant.swift
//  FoodApp
//
//  Created by Macroid on 7/16/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation

struct Restaurant {
    var name:String
    var overAllRating:Double
    var totalRatingCount:Int
    var activeTime:String
    var mobile:String
}
