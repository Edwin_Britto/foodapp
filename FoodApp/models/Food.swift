//
//  Food.swift
//  FoodApp
//
//  Created by Macroid on 7/14/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation

struct Food {
    var id:Int
    var name:String
    var description:String
    var price:Double
    var sessionQuantity:Int
}
