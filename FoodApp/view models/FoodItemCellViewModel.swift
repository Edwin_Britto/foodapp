//
//  FoodItemCellViewModel.swift
//  FoodApp
//
//  Created by Macroid on 7/14/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation

struct FoodItemCellViewModel {
    var food:Food
    
    init(food:Food) {
        self.food = food
    }
}
