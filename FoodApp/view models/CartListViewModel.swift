//
//  CartListViewModel.swift
//  FoodApp
//
//  Created by Macroid on 7/15/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation
import RxSwift

struct CartListViewModel{
    
    var cartList:[Food] = []
    var total:Double = 0
    var foodItemViewType:FoodItemViewType!
    
    init(cartList:[Food]) {
        self.cartList = cartList
    }
    
    mutating func updateFoodByAction(actionType:ActionType,selectedIndex:Int)-> Observable<Food>{
        var food = cartList[selectedIndex]
        switch actionType {
        case .Add:
            food.sessionQuantity = 1
            self.cartList.append(food)
        case .Plus:
            food.sessionQuantity += 1
            self.cartList[selectedIndex] = food
        default:
            food.sessionQuantity -= 1
            if food.sessionQuantity > 0{
                self.cartList[selectedIndex] = food
            }else{
                self.cartList.remove(at: selectedIndex)
            }
        }
        return Observable.create { (observer) -> Disposable in
            observer.onNext(food)
            return Disposables.create {}
        }
    }
    
    func fetchTotalPrice()->Double{
        var total:Double = 0
        for food in cartList{
            total += ((food.sessionQuantity.toDouble() ?? 0) * food.price)
        }
        return total
    }
    
    
}
