//
//  RestaurantCellViewModel.swift
//  FoodApp
//
//  Created by Macroid on 7/16/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation

struct RestaurantCellViewModel {
    var restaurant:Restaurant
    
    init(restaurant:Restaurant) {
        self.restaurant = restaurant
    }
}
