//
//  FoodListViewModel.swift
//  FoodApp
//
//  Created by Macroid on 7/14/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation
import RxSwift

struct FoodListViewModel{
    
    var restaurantInfo:Restaurant = Restaurant(name: "Inka Restaurant", overAllRating: 4,
                                               totalRatingCount: 400, activeTime: "All Days : 09.00 AM - 06.00 PM", mobile: "9854562142")
    var foods:[Food] = []
    var cartList:[Food] = []
    
    //fetch existing food info
    mutating func fetchAllFoods(){
        var foods:[Food] = []
        foods.append(Food(id: 1, name: "Guac de la Costa", description: "tortillas de mais, fruit de la passion, mango", price: 7, sessionQuantity: 0))
        foods.append(Food(id: 2, name: "Chicharron y Cerveza", description: "citron vert / Corona sauce", price: 7, sessionQuantity: 0))
        foods.append(Food(id: 3, name: "Chilitos con", description: "padrones tempura", price: 8, sessionQuantity: 0))

        self.foods = foods
    }
    
    //updating food item
    mutating func updateFoodByAction(actionType:ActionType,food:Food)-> Observable<Food>{
        var food = food
        switch actionType {
        case .Add:
            food.sessionQuantity = 1
            self.cartList.append(food)
        case .Plus:
            food.sessionQuantity += 1
            if let index = cartList.firstIndex(where: {$0.id == food.id}){
                self.cartList[index] = food
            }
        default:
            food.sessionQuantity -= 1
            if let index = cartList.firstIndex(where: {$0.id == food.id}){
                if food.sessionQuantity > 0{
                    self.cartList[index] = food
                }else{
                    self.cartList.remove(at: index)
                }
            }
        }
        return Observable.create { observer -> Disposable in
            observer.onNext(food)
            return Disposables.create {}
        }
    }
    
    mutating func cleanUpCartList(){
        self.cartList.removeAll(where: {$0.sessionQuantity == 0})
    }
    
    //get overall all cart items count
    func getOverAllItemsCount() -> Int{
        var count = 0
        for food in cartList{
            count += food.sessionQuantity
        }
        return count
    }
    
    mutating func updateCartListByFood(food:Food){
        if let index = cartList.firstIndex(where: {$0.id == food.id}){
            self.cartList[index] = food
        }
    }
}
